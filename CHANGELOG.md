# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]
    1. jQuery UI
    2. Ajax
    3. Promises
    4. Backbone JS
    5. Angular JS
    6. Vue.js
    7. React
    8. Google Maps
    9. Cross-browser Layouts
    10. Stomp Over WebSocket 

## [0.0.6] - 2018-07-04
### Added
- Normalize.css

## [0.0.5] - 2018-07-02
### Added
- Semantic Elements
- Link to React + Redux + SVG (Analog Clock)
    
## [0.0.4] - 2018-06-28
### Added
- Media Query, Adaptive Layouts    
- jQuery events
    
## [0.0.3] - 2018-06-26
### Added
- Подключен jQuery, jQueryUI, шрифт Roboto
- добавлены новые иконки
- свёрстаны новые блоки     

## [0.0.2] - 2018-06-25
### Added
- CHANGELOG.md
- иконочный шрифт (+оптимизация svg)
- верстка блоков

### Changed
- README.md

## [0.0.1] - 2018-06-24
### Added
- Initial commit
- Developing Webpage using the next technologies:
    1. GIT
    2. NPM
    3. Gulp
    4. Webpack
    5. Babel
    6. BEM
    7. SASS
    8. Bootstrap mixins
    9. ES6-8
    10. ESLint
    11. StyleLint
    12. .gitignore
    13. Modularity
    14. CSS/JS minimizing
    14. README.md