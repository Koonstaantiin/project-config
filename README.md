В данном репозитории хранится тестовая площадка для разных технологий

Главная ветка, в которой проводится разработка - dev

#### Запуск сервера из директории проекта
php -S 0.0.0.0:8000

Сайт будет доступен по адресу:
http://0.0.0.0:8000/

Шрифты немного другие, так как нет исходника PSD для шаблона

#### Проверенные браузеры
Ubuntu: Mozilla Firefox, Google Chrome

#### Используемые технологии:
1. GIT
2. NPM
3. Gulp
4. Webpack
5. Babel
6. BEM
7. SASS
8. Bootstrap mixins
9. ES6/7/8
10. ESLint
11. StyleLint
12. .gitignore
13. Разбиение на модули
14. README.md
15. CHANGELOG.md
16. https://jakearchibald.github.io/svgomg/ (оптимизация SVG)
17. https://icomoon.io (генерация иконочных шрифтов)
18. jQuery + UI
19. Normalize.css
20. Media Query
21. Adaptive Layout
22. Semantic Elements
23. [React + Redux + SVG (Analog clock)](https://gitlab.com/Koonstaantiin/react-clock)

#### Планируется добавить
1. jQuery UI
2. Ajax
3. Promises
4. Backbone JS
5. Angular JS
6. Vue.js
7. Google Maps
8. Cross-browser Layouts
9. Stomp Over WebSocket 