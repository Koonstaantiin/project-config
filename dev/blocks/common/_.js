"use strict";

export default class TestCommon {
    constructor() {
        this.jQueryProcess();
    }

    jQueryProcess() {
        $(function () {
            $("i[class*='ticon-']").css("opacity", "1");

            $(".hamburger").click(function () {
                $(this).toggleClass('is-active');
                $("body").toggleClass('menu-open');
            });
        });
    }
}

