import Box from "./Box.js";
import TestCommon from "../blocks/common/_.js";

new TestCommon();

const box = new Box();
const predefinedBox = new Box(300, 500, 150);
const errorBox = new Box("fskl", "sddd", "re3l");

const
    a = 1, b = 2, c = 3;

let obj = {
    a,
    b,
    c,
};

console.log(`Box: ${box}`);

console.log(`Error box: ${errorBox}`);

errorBox.width = "150";
errorBox.height = 100;
errorBox.depth = "70e";

console.log(`Error box corrected: ${errorBox}`);

console.log(`Predefined box: ${predefinedBox}`);
console.log(`Predefined box area: ${predefinedBox.getArea()}`);
console.log(`Predefined box volume: ${predefinedBox.getVolume()}`);