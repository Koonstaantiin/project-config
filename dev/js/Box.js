"use strict";

export default class Box {
    constructor(_width = 200, _height = 100, _depth = 50) {
        this._width = this.fixInteger(_width);
        this._height = this.fixInteger(_height);
        this._depth = this.fixInteger(_depth);

        console.log(`Constructor: ${this}`);
    }

    toString() {
        return `Height: ${this._height}. Width: ${this._width}. Depth: ${this._depth}`;
    }

    fixInteger(number) {
        let _number = Math.trunc(number);
        return isNaN(_number) ? 0 : _number;
    }

    get width() {
        return this._width;
    }

    set width(newWidth) {
        this._width = this.fixInteger(newWidth);
    }

    get height() {
        return this._height;
    }

    set height(newHeight) {
        this._height = this.fixInteger(newHeight);
    }

    get depth() {
        return this._depth;
    }

    set depth(newDepth) {
        this._depth = this.fixInteger(newDepth);
    }

    getArea() {
        return this._width * this._height;
    }

    getVolume() {
        return this._width * this._height * this._depth;
    }
}
